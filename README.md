# docker web serveur

Projet de serveur web pour héberger des sites (wordpress) ou des applis sous docker

L'idée est de mettre en place grace à Docker un serveur web permettant d'héberger des sites web (wordpress) ou des applis.
En front end il y aura un reverse proxy nginx qui fera les redirections vers les différentes apps avec un apache en serveur web ou non. En back end tournera un container qui permettra de génerer pour chaque sous domaine un certifcat SSL avec let's encrypt automatiquement à la création du container.

Stack :
- nginx-proxy
- letsencrypt-nginx-proxy-companion
- docker-gen (optionnel)
- wordpress
- mariadb
- phpmyadmin (optionnel)